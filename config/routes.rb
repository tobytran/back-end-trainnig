Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  concern :paginatable do
    get '(page/:page)', action: :index, on: :collection, as: ''
  end

  resources :users, :products, :orders, :product_details, :product_variants, :order_details, :variants, :variant_values, concerns: :paginatable
  post "/login", to: "authencations#login"
end
