class User < ApplicationRecord
    has_secure_password
    has_many :orders
    enum role: [:customer, :admin]
    # before_validation :set_full_name

    # def first_name
    # full_name.split(' ')[0]
    # end

    # def last_name
    # full_name.split(' ')[1]
    # end

    # def set_full_name
    # self.full_name = [first_name, last_name].join(' ')
    # end

end
