class Order < ApplicationRecord
    belongs_to :user
    has_many :order_details, :dependent => :destroy
    has_many :product_details, through: :order_details
    accepts_nested_attributes_for :order_details , allow_destroy: true
end
