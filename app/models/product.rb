class Product < ApplicationRecord
    has_many :product_details
    has_many :product_variants, through: :product_details
    accepts_nested_attributes_for :product_details,  allow_destroy: true
    accepts_nested_attributes_for :product_variants, allow_destroy: true
    scope :filter_by_name, -> (name) {where name: name}
    
end
