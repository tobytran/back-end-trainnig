class OrdersController < ApplicationController
    before_action :authentication
    def index
        orders = Order.page(params[:page]).per(2).includes(:order_details).
                as_json(include: {order_details: {only: [:id, :product_detail_id, :quantity]}})
        page = {
            limit: Product.page(params[:page]).limit_value,
            total_page: Product.page(params[:page]).total_pages,
            current_page: Product.page(params[:page]).current_page,
            next: Product.page(params[:page]).next_page,
            preve: Product.page(params[:page]).prev_page
        }
        render json: {order: orders, page: page}, status: 200
    end

    def show
        if params[:id].match?(/\A\d+\Z/)
            order = Order.includes(:order_details).where("id = ? ",params[:id]).
                    as_json(include: {order_details: {only: [:id, :product_detail_id, :quantity]}})
        else
            order = Order.includes(:order_details,:user).where("users.full_name = ?",params[:id]).references(:user).
                    as_json(include: {order_details: {only: [:id, :product_detail_id, :quantity]}})
        end
       return render json: {'messages': 'Cannot find order'} if  order.blank? 
       render json: order  
    end

    # def show_by_userName
    #     order = Order.includes(:order_details,:user).filter_by_userName(params[:name])
    #             as_json(include: {order_details: {only: [:id, :product_detail_id, :quantity]}})
    #    return render json: {'messages': 'Cannot find order'} if  order.blank? 
    #    render json: order  
    # end

    def create
        order = Order.create(order_params)
        return   render json: {'messages': 'Create order fail'} unless order 
        render json: {'messages': 'Create order Successful'}
    end

    def update
        order = Order.find_by_id(params[:id])
       if order.update(order_params)
        render json: {'messages': 'Update order Successful'}
       else 
        render json: {'messages': 'Update order fail'} 
       end
    end

    def destroy
        order = Order.find_by_id(params[:id])
       if order
            return render json: {'messages': 'Destroy order fail'}  unless order.destroy
            render json: {'messages': 'Destroy order Successful'}  
       else
            render json: {'messages': 'Cannot find order destroy'} 
       end
    end

    private
    def order_params
        {
            user_id: params[:user_id],
            email: params[:email],
            phoneNumber: params[:phoneNumber],
            address: params[:address],
            total: params[:total],
            order_details_attributes: params[:order_details]
        }
        
    end
end


