
class ProductDetailsController < ApplicationController
    def index
        product_details = ProductDetail.includes(:product).as_json(include: :product)
        render json: {product_details: product_details}, status: 200
    end
    def show
        product_detail = ProductDetail.includes(:product).where("id = ?", params[:id]).as_json(include: :product)
        return  render json: {'messages': 'Cannot find product_details'} if  product_detail.blank?
        render json:  product_detail  
    end
    # def create
    #     @product = Product.find_by_id(params[:product_detail][:product_id])
    #     @product_detail = @product.product_details.create(product_detail_params)

    #     @product_variants = @product_detail.product_variants.create(product_variant_params)
    #     binding.pry
    #     if @product_detail
    #         respond_to do |format|
    #             format.json {render json: {'messages': 'Create product detail Successful', 'product': @product_detail}}
    #         end
    #     else 
    #         respond_to do |format|
    #         format.json {render json: {'messages': 'Create product  detail fail'}}
    #         end
    #     end
    # end
    # def update
    #     @product_detail = ProductDetail.find_by_id(params[:id])
    #    if @product_detail.update(product_detail_params)
    #         respond_to do |format|
    #             format.json {render json: {'messages': 'Update product detail Successful','product': @product_detail}}
    #         end
    #    else 
    #         respond_to do |format|
    #             format.json {render json: {'messages': 'Update product detail fail'}}
    #         end
    #    end
    # end
    def destroy
        product_detail = ProductDetail.find_by_id(params[:id])
        if product_detail
            return render json: {'messages': 'Destroy product_detail fail'}  unless product_detail.destroy
            render json: {'messages': 'Destroy product_detail Successful'}  

        else
            render json: {'messages': 'Cannot find product destroy'} 
        end
    end
    # private
    
    # def product_detail_params
    # #   params.permit(:price, product_variannts_attributes: :options)

    #      params.require(:product_detail).permit(:price)
    # end
    # def product_variant_params
    #     params.require(:options).permit(:variant_value_id=>[])["variant_value_id"]
    # end
end
