
class ProductsController < ApplicationController
    before_action :authentication
    def index
        products = Product.page(params[:page]).per(2).includes(product_details: {product_variants: :variant_value}).
        as_json(include:{product_details:{include: {product_variants: {include: :variant_value}}}})
        page = {
            limit: Product.page(params[:page]).limit_value,
            total_page: Product.page(params[:page]).total_pages,
            current_page: Product.page(params[:page]).current_page,
            next: Product.page(params[:page]).next_page,
            preve: Product.page(params[:page]).prev_page
        }
        render json: {product: products, page: page}, status: 200
    end

    def show
        if params[:id].match?(/\A\d+\Z/)
            product = Product.includes(product_details: {product_variants: :variant_value}).where("id = ? ",params[:id]).
                  as_json(include:{product_details:{include: {product_variants: {include: :variant_value}}}})
        else
             product = Product.filter_by_name(params[:id]).includes(product_details: {product_variants: :variant_value}).
                  as_json(include:{product_details:{include: {product_variants: {include: :variant_value}}}})
        end
        return  render json: {'messages': 'Cannot find product'} if product.blank?
        render json: product       
    end
    def create 
        product = Product.create(product_params[:product])
        render json: {'messages': 'Create product Successful'} if product
        render json: {'messages': 'Create product fail'} unless product
    end

    def update
        product = Product.find_by_id(params[:id])
       if product.update(product_params[:product])
        render json: {'messages': 'Update product Successful'} 
       else 
        render json: {'messages': 'Update product fail'} unless product
       end
    end

    def destroy
        product = Product.find_by_id(params[:id])
       if product
            product.destroy
            render json: {'messages': 'Destroy product Successful'} 
       else
            render json: {'messages': 'Cannot find product destroy'} 
       end
    end

    private
    def product_params
        product_params = {
            product: {
                name: params[:name],
                category: params[:category],
                product_details_attributes: params[:product_details]
            }
        } 
    end
end
