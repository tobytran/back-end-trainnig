class ProductPolicy < ApplicationPolicy
    def destroy?
        user.role == "admin"
    end
    def create?
        user.role == "admin"
    end
    def update?
        user.role == "admin"
    end
    
end