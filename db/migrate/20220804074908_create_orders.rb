class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.integer :user_id
      t.string :email
      t.string :phoneNumber
      t.string :address
      t.integer :total
      t.timestamps
    end
  end
end

#Delegate , polymorphic, scope, accepts_nested_attributes_for