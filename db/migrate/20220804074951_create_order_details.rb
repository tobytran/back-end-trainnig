class CreateOrderDetails < ActiveRecord::Migration[7.0]
  def change
    create_table :order_details do |t|
      t.interger :order_id
      t.interger :product_detail_id
      t.integer  :quantity
      t.timestamps
    end
  end
end
