class AddProductDetailIdColumnToOrderDetails < ActiveRecord::Migration[7.0]
  def change
    add_column :order_details, :product_detail_id, :integer
  end
end
