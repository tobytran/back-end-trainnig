class CreateProductVariants < ActiveRecord::Migration[7.0]
  def change
    create_table :product_variants do |t|
      t.interger :product_detail_id
      t.integer  :variant_value_id
      t.timestamps
    end
  end
end
