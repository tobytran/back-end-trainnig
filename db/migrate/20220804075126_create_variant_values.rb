class CreateVariantValues < ActiveRecord::Migration[7.0]
  def change
    create_table :variant_values do |t|
      t.integer :variant_id
      t.string :value
      t.timestamps
    end
  end
end
